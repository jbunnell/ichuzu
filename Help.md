Usage: ichuzu [options] [command]...

Definitions:
 'Key' refers to either an integer index or a text lookup.
 'Name' refers to the text-lookup component of a key.

Options:
 -a    'Name', 'Value'\t\tAdd a new name/value pair
--add
 -l     ±N [optional]\t\tNumbered list existing mappings. +N is first N items,
--list               \t\t-N is the last N items. 0 is all.
 -rm    Key          \t\tRemove a pair by Key.
--remove
 -mv    Key, 'Name'  \t\tMove a pair's Key to a new Name.
--rename
 -d                  \t\tDo a dry run of any commands. Not positionaly dependent.
--dry
 -ssh   'Host', Key  \t\tRun the command bound to Key on the remote Host. Reset host
--remote             \t\tinline by passing a blank string.
 -h                  \t\tShow this message. Will end evaluation and enable -d.
--help

Command:
 A flag or lookup Key for execution.

Notes:
 Flags are executed sequentially. Invalid flags will stop excecution. `--dry` will
 prevent writes to the store, but any commands _before_ `--dry` may have side effects.

 Make sure ichuzu is in your path, and any ssh hosts are in known_hosts.

 Flags with optional arguments should either have their defaults included, or be placed at
 the end of input.

Example Usage:
```sh
 ichuzu --add duckbot 'echo quack!'\\
        --add cowbot 'echo hello, world! | cowsay'\\
        --list 0 --dry\\
        cowbot\\
        --rename cowbot COWBOT\\
        COWBOT\\
        --list\\
        --dry
```
