# ICHUZU
## Version 0.2.0
---
## Introduction

Ichuzu is a command-line utility that dispatches user-defined aliases sequentially.
It's initial inspiration was remotely starting a discord bot with a command line command,
but without adding it to a `bashrc` or `zshrc`.

## Organization
Ichuzu has a descriptive help message in `Help.md` and with `ichuzu --help`. The information
is additionally listed below.

| Command | Short | Args | Description |
|   ---   |  ---  |  --- |     ---     |
| `--add` | `-a`  | `"Name" 'Value'` | Add a key `Name` to the Store with `Value` |
| `--list`| `-l`  | `±N [optional]`   | Display the store, with an optional numbered offset. `0` displays all items |
| `--remove` | `-rm` | `Key` | Remove a pair by Key |
| `--rename` | `-mv` | `Key  "Name"` | Move a pair's Key to a new Name |
| `--dry` | `-d` | - | Do a dry run of all following commands. Any position will stop the Store from saving to file |
| `--remote` | `-ssh` | `"Host"` | Run all following commands on remote Host. Use a blank string to run locally |
| `--help` | `-h` | - | Display a super awesome help message |


## Notes:
 Flags are executed sequentially. Invalid flags will stop excecution. `--dry` will
 prevent writes to the store, but any commands _before_ `--dry` may have side effects.

 Make sure ichuzu is in your path, and any ssh hosts are in known_hosts.

 Flags with optional arguments should either have their defaults included, or be placed at
 the end of input.

## Example Usage:
```sh
 ichuzu --add duckbot 'echo quack!'\
        --add cowbot 'echo hello, world! | cowsay'\
        --list 0 --dry\
        cowbot\
        --rename cowbot COWBOT\
        COWBOT\
        --list\
        --dry
```
Author: Jayson Bunnell. Version: {VERSION}
